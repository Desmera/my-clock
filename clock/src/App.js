import "./Styles/Styles.css";
import CountdownTimer from "./Components/CountdownTime/CountdownTimer";
import DigitalClock from "./Components/DigitalClock/DigitalClock";
import Stopwatch from "./Components/Stopwatch/Stopwatch";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function App() {
  return (
    <div className="app">
      <Container>
        <Row>
          <div className="container">
            <Col>
              <div className="section1">
                <h1>"Time flies like an arrow; fruit flies like a banana."</h1>
                <p> ― Anthony G. Oettinger</p>
              </div>
            </Col>
            <Col>
              <div className="section2">
                <DigitalClock />
                <CountdownTimer />
                <Stopwatch />
              </div>
            </Col>
          </div>
        </Row>
      </Container>
    </div>
  );
}

export default App;
