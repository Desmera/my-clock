import React, { useState, useEffect } from "react";
import "../../Styles/Styles.css";

import "bootstrap/dist/css/bootstrap.min.css";

const Stopwatch = () => {
  const [time, setTime] = useState(0);
  const [timerOn, setTimerOn] = useState(false);

  useEffect(() => {
    let interval = null;

    if (timerOn) {
      interval = setInterval(() => {
        setTime((prevTime) => prevTime + 10);
      }, 10);
    } else {
      clearInterval(interval);
    }

    return () => clearInterval(interval);
  }, [timerOn]);

  return (
    <div className="stopwatch">
      <h3>STOPWATCH</h3>
      <h4>
        <span>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}:</span>
        <span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
        <span>{("0" + ((time / 10) % 100)).slice(-2)}</span>
      </h4>

      <div id="buttons">
        {!timerOn && time === 0 && (
          <button onClick={() => setTimerOn(true)} className="myBtn">
            Start
          </button>
        )}
        {timerOn && (
          <button onClick={() => setTimerOn(false)} className="myBtn">
            Stop
          </button>
        )}
        {!timerOn && time > 0 && (
          <button onClick={() => setTime(0)} className="myBtn">
            Reset
          </button>
        )}
        {!timerOn && time > 0 && (
          <button onClick={() => setTimerOn(true)} className="myBtn">
            Resume
          </button>
        )}
      </div>
    </div>
  );
};
export default Stopwatch;
