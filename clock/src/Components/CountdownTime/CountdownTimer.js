import React, { useState, useEffect } from "react";
import "../../Styles/Styles.css";

const CountdownTimer = () => {
  const [time, setTime] = useState("");

  useEffect(() => {
    let countDownDate = new Date("Dec 31, 2021 23:59:59").getTime();
    let timeToDate = setInterval(() => {
      let now = new Date().getTime();
      let distance = countDownDate - now;
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);

      setTime(days + "d " + hours + "h " + minutes + "m " + seconds + "s");

      if (distance < 0) {
        clearInterval(timeToDate);
        setTime("HAPPY 2022!");
      }
    }, 1000);
  }, []);

  return (
    <div className="countdownTimer">
      <h3>COUNTDOWN TO 2022!</h3>
      <h4>{time}</h4>
    </div>
  );
};

export default CountdownTimer;
