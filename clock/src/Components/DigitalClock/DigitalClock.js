import React, { useState, useEffect } from "react";
import "../../Styles/Styles.css";

const DigitalClock = () => {
  const [clock, setClock] = useState();

  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClock(date.toLocaleTimeString());
    }, 1000);
  }, []);

  return (
    <div className="digital-clock">
      <h3>CURRENT TIME</h3>
      <h4>{clock}</h4>
    </div>
  );
};

export default DigitalClock;
